import os
from flask_marshmallow import Marshmallow
from app.main import create_app, db, register_namespaces, api

app = create_app(os.getenv('BOILERPLATE_ENV') or 'dev')
marsh = Marshmallow(app)
register_namespaces(api)
