'''
DTO's class
'''
from flask_restplus import Namespace, fields


class UserDto:
    '''User DTO'''
    api = Namespace('users', description='user related operations')
    user = api.model('user', {
        'email': fields.String(required=True, description='user email address'),
        'username': fields.String(required=True, description='user username'),
        'password': fields.String(required=True, description='user password'),
        'public_id': fields.String(required=False, description='user Identifier')
    })
    user_update = api.model('user_update', {
        'email': fields.String(required=True, description='user email address'),
        'admin': fields.Boolean(required=False, description='user Admin rights', default=False)
    })


class AuthDto:
    '''Authentication DTO'''
    api = Namespace('auth', description='authentication related operations')
    user_auth = api.model('auth_details', {
        'email': fields.String(required=True, description='The user login'),
        'password': fields.String(required=True, description='The user password')
    })
