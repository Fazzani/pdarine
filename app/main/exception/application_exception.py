'''
Application exceptions
'''

class ApplicationException(Exception):
    '''Base Application Exception'''
class ValidationException(ApplicationException):
    '''Api Validation Model exception'''
