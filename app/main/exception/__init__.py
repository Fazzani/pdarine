'''
Exceptions Hanlers module
'''
from flask import current_app, jsonify
from app.main.exception.application_exception import ApplicationException
from .. import api

@api.errorhandler(ApplicationException)
def handle_application_exception(error):
    message = [str(x) for x in error.args]
    status_code = 500
    success = False
    response = {
        'success': success,
        'error': {
            'type': error.__class__.__name__,
            'message': message
        }
    }
    current_app.logger.warn(f'In Error handler: {error}')

    return jsonify(response), status_code


@api.errorhandler(Exception)
def handle_unexpected_error(error):
    status_code = 500
    success = False
    response = {
        'success': success,
        'error': {
            'type': 'UnexpectedException',
            'message': 'An unexpected error has occurred.'
        }
    }

    current_app.logger.warn(f'In Error handler Exception {error}')
    return jsonify(response), status_code
