"""
User Controller
"""
from typing import Any, Union
from flask import request, current_app
from flask_restplus import Resource

from app.main.model.user import User, user_schema
from ..util.dto import UserDto
from ..service.user_service import save_user, get_all_users, get_user, update_user, remove_user

API = UserDto.api


@API.route('/')
class UserList(Resource):
    @API.doc('list_of_registered_users', security='apikey')
    def get(self):
        """List all registered users"""
        current_app.logger.info(f'Fetching All users')
        return user_schema.jsonify(get_all_users(), many=True)

    @API.response(201, 'User successfully created.')
    @API.doc('create a new user')
    @API.expect(UserDto.user, validate=True)
    def post(self) -> Any:
        """Created a new user"""
        data = request.json
        user: User = user_schema.make_user(data)
        user.admin = False
        return save_user(user)


@API.route('/<string:public_id>')
@API.doc(params={'public_id': 'The User identifier'})
@API.doc(responses={
    200: 'Success', 404: 'User not found.'})
class UserAPI(Resource):
    @API.marshal_with(UserDto.user)
    def get(self, public_id):
        """get a user given its identifier"""
        current_app.logger.info(f'Fetching User {public_id}')
        user: User = get_user(public_id)
        if not user:
            API.abort(404)
        else:
            return user

    def delete(self, public_id) -> Union[Any]:
        """get a user given its identifier"""
        current_app.logger.info(f'Deleting User {public_id}')

        return remove_user(public_id)

    @API.expect(UserDto.user_update, validate=True)
    def put(self, public_id) -> Union[Any]:
        """get a user given its identifier"""
        data = request.get_json()
        current_app.logger.info(f'data => {data} ')
        current_app.logger.info(f'Updating User {public_id}')
        return update_user(public_id, data)
