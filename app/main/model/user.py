import datetime

import jwt

from app import marsh
from ..model.blacklist import BlacklistToken
from ..exception import ApplicationException
from marshmallow import post_load
from .. import db, flask_bcrypt
from ..config import key

class User(db.Model):
    """User Model for storing user related details"""
    __tablename__ = "user"

    id: str = db.Column(db.Integer, primary_key=True, autoincrement=True)
    email: str = db.Column(db.String(255), unique=True, nullable=False)
    registered_on: datetime = db.Column(
        db.DateTime, nullable=False, default=datetime.datetime.utcnow)
    admin = db.Column(db.Boolean, nullable=False, default=False)
    public_id = db.Column(db.String(100), unique=True)
    username = db.Column(db.String(50), unique=True)
    password_hash = db.Column(db.String(100))

    @property
    def password(self):
        raise AttributeError('password: write-only field')

    @password.setter
    def password(self, password: str):
        self.password_hash = flask_bcrypt.generate_password_hash(password)

    def check_password(self, password):
        return flask_bcrypt.check_password_hash(self.password_hash, password)

    def __repr__(self):
        return "<User '{}'>".format(self.username)

    def encode_auth_token(self, user_id: str):
        """
        Generates the Auth token
        :return: string
        """
        try:
            payload = {
                'exp': datetime.datetime.utcnow() + datetime.timedelta(days=1, seconds=5),
                'iat': datetime.datetime.utcnow(),
                'sub': user_id
            }
            return jwt.encode(payload, key, algorithm='HS256')
        except ApplicationException as e:
            return e

    @staticmethod
    def decode_auth_token(auth_token: str):
        """
        Decodes the auth token
        :param auth_token:
        :return: integer|string
        """
        try:
            payload = jwt.decode(auth_token, key)
            is_blacklisted_token = BlacklistToken.check_blacklist(auth_token)
            if is_blacklisted_token:
                return 'Token blacklisted. Please log in again.'
            else:
                return payload['sub']
        except jwt.ExpiredSignatureError:
            return 'Signature expired. Please log in again.'
        except jwt.InvalidTokenError:
            return 'Invalid token. Please log in again.'


class UserSchema(marsh.ModelSchema):
    class Meta:
        model = User

    @post_load
    def make_user(self, data, **kwargs):
        return User(**data)

user_schema = UserSchema()
