'''
Application init
'''
import os
from flask import request, Blueprint, Flask
from sqlalchemy import MetaData
from flask_restplus import Api
from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt
from .config import config_by_name


_org_url: str = os.environ.get('OKTA_ORG_URL')
_token_url: str = f'{_org_url}/oauth2/default/v1/token'
_auth_url: str = f'{_org_url}/oauth2/v1/authorize'

authorizations = {
    'apikey': {
        'type': 'apiKey',
        'in': 'header',
        'name': 'X-API'
    },
    'oauth2': {
        'type': 'oauth2',
        'flow': 'accessCode',
        'tokenUrl': _token_url,
        'authorizationUrl': _auth_url,
        'redirect_uri': 'http://localhost:3330/authorization-code/callback',
        'scopes': {
            'read': 'Grant read-only access',
            'write': 'Grant read-write access',
        }
    }
}

naming_convention = {
    "ix": 'ix_%(column_0_label)s',
    "uq": "uq_%(table_name)s_%(column_0_name)s",
    "ck": "ck_%(table_name)s_%(column_0_name)s",
    "fk": "fk_%(table_name)s_%(column_0_name)s_%(referred_table_name)s",
    "pk": "pk_%(table_name)s"
}
db = SQLAlchemy(metadata=MetaData(naming_convention=naming_convention))

flask_bcrypt = Bcrypt()

blueprint = Blueprint('api', __name__, url_prefix='/api')

api = Api(blueprint,
          title='Flask RESPLUS API BOILER-PLATE WITH JWT',
          version='1.0',
          description='a boilerplate for flask restplus web service',
          validate=True,
          authorizations=authorizations,
          security=['apikey', {'oauth2': 'read'}]
          )


def register_namespaces(pdarineApi: Api) -> None:
    '''Flask namespaces registration'''
    from .controller.auth_controller import api as auth_ns
    from .controller.user_controller import API as user_ns
    pdarineApi.add_namespace(user_ns, path='/users')
    pdarineApi.add_namespace(auth_ns, path='/auth')


def register_exceptions() -> None:
    '''Global exceptions handlers registration'''
    from app.main.exception import application_exception


def create_app(config_name: str = 'dev') -> Flask:
    ''' APP factory '''
    app = Flask(__name__)
    app.config.from_object(config_by_name[config_name])
    db.init_app(app)
    flask_bcrypt.init_app(app)
    app.register_blueprint(blueprint)
    app.url_map.strict_slashes = False
    app.app_context().push()
    register_exceptions()
    return app
