'''
User service module
'''
from app.main import db
from app.main.model.user import User
from app.main.exception import ApplicationException


def save_user(user: User):
    db_user = User.query.filter_by(email=user.email).first()
    if not db_user:
        save_changes(user)
        # response = jsonify(new_user.to_dict())
        # response.status_code = 201
        # response.headers['Location'] = url_for('api.get_user', id=new_user.id)
        return generate_token(user)
    else:
        response_object = {
            'status': 'fail',
            'message': 'User already exists. Please Log in.'
        }
        return response_object, 409


def get_all_users():
    # from app.main.exception.application_exception import ApplicationException
    # raise ApplicationException('oooops')
    return User.query.all()


def get_user(public_id: str):
    return User.query.filter_by(public_id=public_id).first()


def remove_user(public_id: str):
    user = User.query.filter_by(public_id=public_id).first()
    if user:
        db.session.delete(user)
        response_object = {
            'status': 'success',
            'message': f'User {public_id} was deleted successfully'
        }
        return response_object, 200
    else:
        response_object = {'status': "fail",
                           "message": f"User {public_id} not found!"}
    db.session.commit()
    return response_object, 400


def update_user(public_id: str, userInput: User):
    user: User = User.query.filter_by(public_id=public_id).first()
    if user:
        user.admin = userInput['admin']
        user.email = userInput['email']
        response_object = {
            'status': 'success',
            'message': f'User {public_id} was updated successfully'
        }
        return response_object, 200
    else:
        response_object = {'status': "fail",
                           "message": f"User {public_id} not found!"}
    db.session.commit()
    return response_object, 400


def generate_token(user):
    try:
        auth_token = user.encode_auth_token(user.id)
        response_object = {
            'status': 'success',
            'message': 'Successfully registered.',
            'Authorization': auth_token.decode()
        }
        return response_object, 201
    except ApplicationException as e:
        response_object = {
            'status': 'fail',
            'message': 'Some error occurred. Please try again'
        }
        return response_object, 401


def save_changes(data):
    db.session.add(data)
    db.session.commit()
