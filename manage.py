'''
Project main module
'''
import logging
import logging.config
# import os
import unittest
from os import environ

import yaml
from flask import Flask, g
from flask_migrate import Migrate, MigrateCommand
from flask_oidc import OpenIDConnect
from flask_script import Manager
from okta import UsersClient

from app.main import db
from app import app

def _configure_oicd(app: Flask) -> OpenIDConnect:
    '''Open ID connection configuration'''
    app.config["OIDC_CLIENT_SECRETS"] = "client_secrets.json"
    app.config["OIDC_COOKIE_SECURE"] = False
    app.config["OIDC_CALLBACK_ROUTE"] = "/oidc/callback"
    app.config["OIDC_SCOPES"] = ["openid", "email", "profile"]
    app.config["SECRET_KEY"] = "sqdljqlqizaazq*af+vuay*q$achv"
    return OpenIDConnect(app)


logging.config.dictConfig(yaml.safe_load(open(('logging.yml'))))
oidc = _configure_oicd(app)
app.app_context().push()
manager = Manager(app)
migrate = Migrate(app, db, render_as_batch=True)
manager.add_command('db', MigrateCommand)
okta_client = UsersClient(environ.get("OKTA_ORG_URL"),
                          environ.get("OKTA_AUTH_TOKEN"))


@app.before_request
def before_request():
    if oidc.user_loggedin:
        g.user = okta_client.get_user(oidc.user_getfield("sub"))
    else:
        g.user = None


@manager.command
@manager.option('-p', '--port', dest='port', help='Server PORT', default=3330)
def run(port=3330):
    """
    marks the function as executable from the command line
    """
    app.run(port=port)


@manager.command
def test():
    """Runs the unit tests."""
    tests = unittest.TestLoader().discover('app/test', pattern='test*.py')
    result = unittest.TextTestRunner(verbosity=2).run(tests)
    if result.wasSuccessful():
        return 0
    return 1


if __name__ == '__main__':
    manager.run()
