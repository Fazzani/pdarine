# pDarine (MY First flask application)

[![pipeline status](https://gitlab.com/fazzani/pdarine/badges/master/pipeline.svg)](https://gitlab.com/fazzani/pdarine/commits/master)

Inspired from this [tutorial][tuto_ref]

## Database migration

```shell
# init migration for the first time
python manage.py db init
# create new migration
python manage.py db migrate --message 'initial database migration'
# upgrade database to the last migration
python manage.py db upgrade
```

## Python tips

une variable initialisée dans le fichier \__init__ d'un **module** est considérée comme **Singleton**

### package exports

The \__init__.py file can also decide which modules the package exports as the API, while keeping other modules internal, by overriding the \__all__ variable, like so:

```python
__init__.py:

__all__ = ["bar"]
```

### Explore lib

```python
>>> import urllib
>>> dir(urllib)
```

### Extending module load path

```python
PYTHONPATH=/foo python game.py
# or by code
sys.path.append("/foo")
```

### Multithreading

- concurrent.futures : le module propose une interface similaire pour paralléliser avec des threads ou des processus. La création des threads s’écrit plus rapidement.

- asyncio : ce module fonctionne avec les mots-clés async, await et il est particulièrement adapté à la parallélisation à des accès aux ressources.

## HAVE TO SEE

- [Blueprints](https://flask.palletsprojects.com/en/1.1.x/blueprints/#blueprints)
- [FLask namespaces](https://blog.invivoo.com/designer-des-apis-rest-avec-flask-restplus/)
- [Comment bien structurer une API Flask](https://www.freecodecamp.org/news/structuring-a-flask-restplus-web-service-for-production-builds-c2ec676de563/)

[tuto_ref]: https://www.freecodecamp.org/news/structuring-a-flask-restplus-web-service-for-production-builds-c2ec676de563/
